import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Card } from "./card.model";
import { Store } from "@ngrx/store";
import * as fromApp from "../store/app.reducer";
import * as CardAction from "./store/card.action";
import { Observable } from "rxjs";
import { Router } from "@angular/router";
@Component({
  selector: "app-add-card",
  templateUrl: "./add-card.component.html",
  styleUrls: ["./add-card.component.css"],
})
export class AddCardComponent implements OnInit {
  card: Observable<{ card: Card[] }>;

  constructor(private Store: Store<fromApp.AppState>, private router: Router) {}
  form: FormGroup;
  ngOnInit(): void {
    this.inItForm();
  }
  private inItForm() {
    let card_number = "";
    let card_Holder = "";
    let expiration_date = "";
    let security_code = "";
    let amount = "";

    this.form = new FormGroup({
      card_number: new FormControl(card_number, [
        Validators.required,
        Validators.pattern(/^[0-9]{10,16}$/),
      ]),
      card_Holder: new FormControl(card_Holder, [
        Validators.required,
        Validators.pattern(/^[a-zA-Z]*$/),
        Validators.minLength(3),
        Validators.maxLength(10),
      ]),
      expiration_date: new FormControl(expiration_date, [
        Validators.required,
        this.customValidater.bind(this),
      ]),
      security_code: new FormControl(security_code, [
        Validators.pattern(/^[0-9]{3,3}$/),
        // Validators.maxLength(3),
      ]),
      amount: new FormControl(amount, Validators.required),
    });
  }
  onSubmit() {
    const data = this.form;
    const newCard = new Card(
      data.value.card_number,
      data.value.card_Holder,
      data.value.expiration_date,
      data.value.security_code,
      data.value.amount
    );
    this.Store.dispatch(new CardAction.AddCard(newCard));
    this.router.navigate(["/"]);
    this.card = this.Store.select("card");
    this.form.reset();
  }

  customValidater(control: FormControl) {
    var CurrentDate = new Date();
    var GivenDate = new Date(control.value);
    if (GivenDate > CurrentDate) {
      return null;
    } else {
      return { nameIsForbidden: true };
    }
  }
}
