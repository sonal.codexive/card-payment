export class Card {
  public card_number: string;
  public card_Holder: string;
  public expiration_date: Date;
  public security_code: string;
  public amount: number;

  constructor(
    card_number: string,
    card_Holder: string,
    expiration_date: Date,
    security_code: string,
    amount: number
  ) {
    this.card_number = card_number;
    this.card_Holder = card_Holder;
    this.expiration_date = expiration_date;
    this.security_code = security_code;
    this.amount = amount;
  }
}
