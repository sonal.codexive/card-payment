import { Action } from "@ngrx/store";
import { Card } from "../card.model";

export const ADD_CARD = "ADD_CARD";

export class AddCard implements Action {
  readonly type = ADD_CARD;
  constructor(public payload: Card) {}
}

export type CardActions = AddCard;
