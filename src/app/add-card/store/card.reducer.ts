import { Card } from "../card.model";
import * as CardActions from "./card.action";

export interface State {
  card: Card[];
  Index: number;
  editcard: Card;
}
const initialState: State = {
  card: [],
  Index: -1,
  editcard: null,
};
export function cardReducer(
  state: State = initialState,
  action: CardActions.CardActions
) {
  switch (action.type) {
    case CardActions.ADD_CARD:
      // console.log("action", action.payload);
      // console.log("card", state.card);
      return {
        ...state,
        card: [...state.card, action.payload],
      };

    default:
      return state;
  }
}
