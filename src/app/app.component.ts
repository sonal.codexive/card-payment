import { Component, OnInit } from "@angular/core";
import { Card } from "./add-card/card.model";
import { Store } from "@ngrx/store";
import * as fromApp from "./store/app.reducer";
import { Observable } from "rxjs";
import { NavigationEnd, Router } from "@angular/router";
import { PaymentService } from "./services/payment.service";
declare var $: any;
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit {
  title = "card-payment";
  url;
  card: Observable<{ card: Card[] }>;
  constructor(
    private Store: Store<fromApp.AppState>,
    private router: Router,
    private payment: PaymentService
  ) {
    this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        this.url = e.url;
      }
    });
  }
  ngOnInit(): void {
    this.card = this.Store.select("card");
  }

  onPayment(item: Card) {
    this.payment.storeRecipe(item).subscribe((respones) => {
      // console.log("data", respones);
      $(".toast").toast("show");
    });
  }
}
