import { BrowserModule } from "@angular/platform-browser";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import * as fromApp from "./store/app.reducer";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { AddCardComponent } from "./add-card/add-card.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { StoreModule } from "@ngrx/store";
import { PaymentService } from "./services/payment.service";

@NgModule({
  declarations: [AppComponent, AddCardComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    StoreModule.forRoot(fromApp.appReducer),
  ],
  providers: [PaymentService],
  bootstrap: [AppComponent],
})
export class AppModule {}
