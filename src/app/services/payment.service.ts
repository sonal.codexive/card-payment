import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
@Injectable({
  providedIn: "root",
})
export class PaymentService {
  constructor(private http: HttpClient) {}

  storeRecipe(item) {
    return this.http.post("https://jsonplaceholder.typicode.com/posts", item);
  }
}
