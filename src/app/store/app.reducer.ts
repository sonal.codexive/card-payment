import * as card from "../add-card/store/card.reducer";

import { ActionReducerMap } from "@ngrx/store";
export interface AppState {
  card: card.State;
}
export const appReducer: ActionReducerMap<AppState> = {
  card: card.cardReducer,
};
